import React from 'react';

const Starter = React.lazy(() => import('./views/Starter/Starter.js'));
const ProjectOverview = React.lazy(() => import('./views/ProjectOverview/ProjectOverview.js'));
const SingleProject = React.lazy(() => import('./views/SingleProject/SingleProject.js'));
const SingleUnit = React.lazy(() => import('./views/SingleUnit/SingleUnit.js'));
const SubUser = React.lazy(() => import('./views/SubUser/SubUser.js'));
const ProfilePage = React.lazy(() => import('./views/ProfilePage/ProfilePage.js'));


// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: '/starter', exact: true, name: 'Starter Dashboard', component: Starter },
  { path: '/projects', exact: true, name: 'Projekte', component: ProjectOverview },
  { path: '/projects/:id', exact: true, name: 'Projekt', component: SingleProject },
  { path: '/projects/:id/:unitid', exact: true, name: 'Wohneinheit', component: SingleUnit },
  { path: '/subusers', exact: true, name: 'Benutzer', component: SubUser },
  { path: '/profile', exact: true, name: 'Profil', component: ProfilePage },
  { path: '/', exact: true, name: 'Home' },

];

export default routes;
