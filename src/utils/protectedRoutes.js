import React from 'react'
import { Redirect } from 'react-router-dom'

class ProtectedRoute extends React.Component {

    render() {
        const Render = this.props.render;
        const isAuthenticated = localStorage.getItem('user');
       
        return isAuthenticated ? (
            <Render />
        ) : (
            <Redirect to={{ pathname: '/login' }} />
        );
    }
}

export default ProtectedRoute;