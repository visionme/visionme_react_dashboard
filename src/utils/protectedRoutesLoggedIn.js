import React from 'react'
import { Redirect } from 'react-router-dom'

class ProtectedRouteLoggedIn extends React.Component {

    render() {
        const Render = this.props.render;
        const isAuthenticated = localStorage.getItem('user');
       
        return isAuthenticated ? (
            <Redirect to={{ pathname: '/' }} />
        ) : (
            <Render />
        );
    }
}

export default ProtectedRouteLoggedIn;