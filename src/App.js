import React, { Component } from 'react';
import { HashRouter, Route, Switch } from 'react-router-dom';
import ProtectedRoute from './utils/protectedRoutes'
import ProtectedRouteLoggedIn from './utils/protectedRoutesLoggedIn'
// import { renderRoutes } from 'react-router-config';
import './App.scss';

const loading = () => <div className="animated fadeIn pt-3 text-center">Loading...</div>;

// Containers
const DefaultLayout = React.lazy(() => import('./containers/DefaultLayout'));

// Pages
const Login = React.lazy(() => import('./views/Pages/Login'));
const Register = React.lazy(() => import('./views/Pages/Register'));
const Page404 = React.lazy(() => import('./views/Pages/Page404'));
const Page500 = React.lazy(() => import('./views/Pages/Page500'));

class App extends Component {

  componentDidMount(){
    if(localStorage.getItem('user')) {
      const user = JSON.parse(localStorage.getItem('user'))
      this.setState({user})
    }
  }

  render() {
    return (
      <HashRouter>
          <React.Suspense fallback={loading()}>
            <Switch>
              <Route exact path="/login" name="Login Page" render={props => <Login {...props}/>} />
              <Route exact path="/register" name="Register Page" render={props => <Register {...props}/>} />
              <ProtectedRoute exact path="/500" name="Page 500" render={props => <Page500 {...props}/>} />
              <ProtectedRoute path="/404" name="Page 404" render={props => <Page404 {...props}/>} />
              <ProtectedRoute path="/" name="Home" render={props => <DefaultLayout {...props}/>} />
              <ProtectedRoute render={props => <DefaultLayout {...props}/>} />
            </Switch>
          </React.Suspense>
      </HashRouter>
    );
  }
}

export default App;
