import React, { Component, lazy, Suspense } from 'react';
import axios from 'axios';
import {API_URL} from '../../utils/urls';
import {Link} from 'react-router-dom';
import { Bar, Line } from 'react-chartjs-2';
import {
  Badge,
  Button,
  ButtonDropdown,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  CardTitle,
  Col,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Progress,
  Row,
  Table,
} from 'reactstrap';

import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities'

const brandPrimary = getStyle('--primary')
const brandSuccess = getStyle('--success')
const brandInfo = getStyle('--info')
const brandWarning = getStyle('--warning')
const brandDanger = getStyle('--danger')


class ProjectOverview extends Component {
    constructor(props) {
      super(props);
  
      this.state = {
        user: '',
        token: '',
        projects: ''
      };
    }
  
    // Start
    componentDidMount() {
      this.getProjectsFromApi();
      const { history } = this.props
    }

    getProjectsFromApi = async () => {
      const userObject = JSON.parse(localStorage.getItem('user'));
      
      try{
        const projectsRes = await axios({
          method: 'GET',
          url: `${API_URL}/projects?user=${userObject.user.id}`,
          headers: {
            'Authorization': `Bearer ${userObject.jwt}`
          }
        })
        
        console.log("ProjectOverview.js getProjectsFromApi", projectsRes.data)
  
        this.setState({
          projects: projectsRes.data
        }, () => {
          console.log('projects:' + JSON.stringify(this.state.projects))
        })
      } 
      catch(error) {
        console.log(error)
      }
    }

    loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>
  
    render() {
      var projectListItems = ''
      if(typeof this.state.projects === typeof [1,2]) {
        projectListItems = this.state.projects.map(project => {
          return (
            <tr key={project.id}>
              <th scope="row" className="text-left">
                <div>
                  <img src={`${API_URL}${project.thumbnail.url}`} height="100" alt=""/>
                </div>
              </th>
              <td>
                <div>{project.name}</div>
                <div className="small text-muted">
                  <span>Neu</span> | Erstellt: {project.createdAt}
                </div>
              </td>
              <td>
                <div className="clearfix">
                  <div className="text-center">
                    <strong>{project.units.length}</strong>
                  </div>
                </div>
              </td>
              <td className="text-center">
                <Badge className="mr-1" color="success">Verfügbar</Badge>
              </td>
              <td>
                <Button block color="primary" size="sm" onClick={() => {
                  window.open(`/#/projects/${project.id}`, "_self")
                }}>Öffnen</Button>
                <Button block color="primary" size="sm">Bearbeiten</Button>
                <Button block color="primary" size="sm">Archivieren</Button>
              </td>
            </tr>
          )
        })
      }

      var projectCards = '';
      if(typeof this.state.projects === typeof [1,2]) {
        projectCards = this.state.projects.map(project => {
          return (
            <Col xs="12" sm="6" md="6" lg="6" xl="4">
              <Card>
                <img alt="Card image cap" className="card-img-top img-fluid" src={`${API_URL}${project.thumbnail.url}`} />
                <CardBody>
                  <Row>
                    <Col>
                      <h4 className="card-title">{project.name}</h4>
                      <p>{project.description}</p>
                    </Col>
                  </Row>
                  <Row>
                    <Col col="12" sm="4" md="6" lg="7" xl="8" className="mb-3 mb-xl-0 text-left">
                    <Badge className="mr-1" color="success">Verfügbar</Badge>
                    </Col>
                    <Col col="12" sm="8" md="6" lg="5" xl="4" className="mb-3 mb-xl-0">
                      <Button block color="primary" size="sm" onClick={() => {
                        window.open(`/#/projects/${project.id}`, "_self")
                      }}>Öffnen</Button>
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </Col>
          )
        })
      }
      
      

      return (
        <div className="animated fadeIn">
        
        <Row>
          {projectCards}

          <Col xs="12" sm="6" md="6" lg="6" xl="4">
              <Card>
                <img alt="" className="card-img-top img-fluid" src=''>
                </img>
                <CardBody>
                  <Row>
                    <Col>
                      <h4 className="card-title"></h4>
                      <p></p>
                    </Col>
                  </Row>
                  <Row>
                    <Col col="12" sm="0" md="0" lg="0" xl="6" className="mb-3 mb-xl-0 text-left">
                    </Col>
                    <Col col="12" sm="12" md="12" lg="12" xl="6" className="mb-3 mb-xl-0">
                      <Button block color="primary" size="sm" onClick={() => {
                        window.open(`/#/projects/`, "_self")
                      }}>Neues Projekt erstellen</Button>
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </Col>
        </Row>
        </div>
    );
  }
}

export default ProjectOverview;
