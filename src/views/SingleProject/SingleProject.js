import React, { Component, lazy, Suspense, useState } from 'react';
import axios from 'axios'
import {API_URL} from '../../utils/urls'
import { handleChange } from '../../utils/inputs'
import { Bar, Line } from 'react-chartjs-2';
import {
  Badge,
  Button,
  ButtonDropdown,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  CardTitle,
  Col,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Progress,
  Row,
  Table,


  Modal, ModalBody, ModalFooter, ModalHeader,
  
  Collapse,
  Fade,
  Form,
  FormGroup,
  FormText,
  FormFeedback,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButtonDropdown,
  InputGroupText,
  Label,

} from 'reactstrap';



import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities'
import Axios from 'axios';

const brandPrimary = getStyle('--primary')
const brandSuccess = getStyle('--success')
const brandInfo = getStyle('--info')
const brandWarning = getStyle('--warning')
const brandDanger = getStyle('--danger')


class SingleProject extends Component {
    constructor(props) {
      super(props);
  
      this.state = {
        // Modal states
        modal: false,

        // File Upload Url state
        fileUrl: null,
        file: null,

        // Other states
        user: '',
        token: '',
        project: '',
        units: '',

        // new projects data
        updatedProjectName: '',
        updatedProjectDescription: '',
      };

      this.toggle = this.toggle.bind(this);
      this.handleFileUploadChange = this.handleFileUploadChange.bind(this);
      this.handleChange = handleChange.bind(this);
    }
    
    toggle() {
      this.setState({
        modal: !this.state.modal,
      });
    }

    // Start
    componentDidMount() {
      this.getProjectsFromApi();
      const { history } = this.props
    }

    getProjectsFromApi = async () => {
      const userObject = JSON.parse(localStorage.getItem('user'));
      
      try{
        const projectRes = await axios({
          method: 'GET',
          url: `${API_URL}/projects/${this.props.match.params.id}`,
          headers: {
            'Authorization': `Bearer ${userObject.jwt}`
          }
        })
        
        console.log("SingleProject.js getProjectFromApi", projectRes.data)
  
        this.setState({
          project: projectRes.data,
          units: projectRes.data.units,
          updatedProjectName: projectRes.data.name,
          updatedProjectDescription: projectRes.data.description,
        }, () => {
          console.log('project:' + JSON.stringify(this.state.project))
        })
      } 
      catch(error) {
        console.log(error)
      }
    }

    handleFileUploadChange(event) {
      this.setState({
        fileUrl: URL.createObjectURL(event.target.files[0]),
        file: event.target.files[0],
      })
    }
    
    updateProjectViaApi = async () => {
      const userObject = JSON.parse(localStorage.getItem('user'));
      
      // construct new project json object
      //const updatedProject = new FormData();
      //updatedProject.append('name', this.state.updatedProjectName);
      //updatedProject.append('description', this.state.updatedProjectDescription);
      
      const updatedProject = {
        name: this.state.updatedProjectName,
        description: this.state.updatedProjectDescription,
      }
      
      // construct new preview image json object
      const formData = new FormData();
      formData.append('files', this.state.file);
      formData.append('ref', 'project');
      formData.append('refId', this.state.project.id);
      formData.append('field', 'thumbnail');


      console.log('Updated Project Object: ', updatedProject)
      console.log('Form data object: ', formData)

      try{
        // Update Project info
        console.log('POST: update product')
        const updateProjectRes = await Axios({
          method: 'PUT',
          url: `${API_URL}/projects/${this.state.project.id}`,
          headers: {
            'Authorization': `Bearer ${userObject.jwt}`
          },
          data: updatedProject,
        })
        console.log("SingleProject.js updateProjectViaApi Response: ", updateProjectRes.data);
        this.setState({
          project: updateProjectRes.data
        });
        


        // Update project preview image
        if(this.state.file) {
          console.log('POST: update product thumbnail')

          const updateImageRes = await Axios({
            method: 'POST',
            url: `${API_URL}/upload`,
            headers: {
              'Authorization': `Bearer ${userObject.jwt}`
            },
            data: formData,
          })

          console.log('UpdateImage Response: ', updateImageRes)
          window.location.reload(false);
        }


        this.toggle();
      } catch(error) {
        console.log('Error: ', error.message);
      }
    }

    loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>
  
    render() {
      const {updatedProjectName, updatedProjectDescription} = this.state
      var projectListItems = ''
      if(typeof this.state.units === typeof [1,2]) {
        projectListItems = this.state.units.map(unit => {
          return (
            <tr key={unit.id} className="table-bordered">
              <td className="text-center">
                <div>
                  <img src={unit.thumbnail ? `${API_URL}${unit.thumbnail.url}` : ''} height="150" className="" alt=""/>
                </div> 
              </td>
              <td>
                <div><p><b>{unit.name}</b></p></div>
                <div><p>{unit.description}</p></div>
                <div className="small text-muted">
                  <span>Neu</span> | Erstellt: {unit.createdAt}
                </div>
              </td>
              <td>
                <div className="clearfix">
                  <div className="text-center">
                    <strong>{unit.variants ? unit.variants.length : 0}</strong>
                  </div>
                </div>
              </td>
              <td className="text-center">
                <Badge className="mr-1" color="success">Verfügbar</Badge>
              </td>
              <td>
                <Button block color="primary" size="sm" onClick={() => {
                  //window.open('/#/starter', "_self")
                  window.open(`/#/projects/${this.state.project.id}/${unit.id}`, "_self")
                }}>Öffnen</Button>
              </td>
            </tr>
          )
        })
      }
      


      var unitCards = '';
      if(typeof this.state.units === typeof [1,2]) {
        unitCards = this.state.units.map(unit => {
          return (
            <Col xs="12" sm="6" md="6" lg="6" xl="4">
              <Card>
                <img alt="" className="card-img-top img-fluid" src={unit.thumbnail ? `${API_URL}${unit.thumbnail.url}` : ''} />
                <CardBody>
                  <Row>
                    <Col>
                      <h4 className="card-title">{unit.name}</h4>
                      <p>{unit.description}</p>
                    </Col>
                  </Row>
                  <Row>
                    <Col col="12" sm="4" md="6" lg="7" xl="8" className="mb-3 mb-xl-0 text-left">
                    <Badge className="mr-1" color="success">Verfügbar</Badge>
                    </Col>
                    <Col col="12" sm="8" md="6" lg="5" xl="4" className="mb-3 mb-xl-0">
                      <Button block color="primary" size="sm" onClick={() => {
                        window.open(`/#/projects/${this.state.project.id}/${unit.id}`, "_self")
                      }}>Öffnen</Button>
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </Col>
          )
        })
      }



      return (
        <div className="animated fadeIn">

          <Row>
            <Col>
              <Card>
                <CardHeader>
                  Projekt <b>{this.state.project.name}</b>
                </CardHeader>
                <CardBody>
                  <Row>
                    <Col xs="7" xl="8">
                      <Row>
                        <Col xs="4" xl="3">
                          <div>
                            <p>
                              <b>Name:</b>
                            </p>
                          </div>
                        </Col>
                        <Col xs="8" xl="9">
                          <div>
                            <p>
                            {this.state.project.name ? this.state.project.name : "Für diese Wohneinheit wurde kein Name hinterlegt." }
                            </p>
                          </div>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="4" xl="3">
                          <div>
                            <p>
                            <b>Beschreibung:</b>
                            </p>
                          </div>
                        </Col>
                        <Col xs="8" xl="9">
                          <div>
                            <p>
                              {this.state.project.description ? this.state.project.description : "Für diese Wohneinheit wurde keine Beschreibung hinterlegt.    " }
                            </p>
                          </div>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="4" xl="3">
                          <div>
                            <p>
                            <b>Erstellt am:</b>
                            </p>
                          </div>
                        </Col>
                        <Col xs="8" xl="9">
                          <div>
                            <p>
                              {this.state.project.createdAt ? this.state.project.createdAt : 'Keine Angabe.'}
                            </p>
                          </div>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="4" xl="3">
                          <div>
                            <p>
                              <b>Wohneinheiten:</b>
                            </p>
                          </div>
                        </Col>
                        <Col xs="8" xl="9">
                          <div>
                            <p>
                              {this.state.project.units ? this.state.project.units.length : 0}
                            </p>
                          </div>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="4" xl="3">
                          <div>
                            <p>
                              <b>Status:</b>
                            </p>
                          </div>
                        </Col>
                        <Col xs="8" xl="9">
                          <Badge className="mr-1" color="success">Verfügbar</Badge>
                        </Col>
                      </Row>
                    </Col>
                    <Col xs="5" xl="4" className="text-right overflow">
                      <div>
                        <img src={this.state.project.thumbnail ? `${API_URL}${this.state.project.thumbnail.url}` : ''} height="250" className="border overflow-hidden" alt=""/>
                      </div>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <Button color="primary" onClick={this.toggle} className="mr-1">Bearbeiten</Button>
                  <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                    <ModalHeader toggle={this.toggle}>Projekt bearbeiten</ModalHeader>
                    <ModalBody>
                      <Row>
                        <Col xs="12" md="12">
                              <Form onSubmit={this.updateProjectViaApi} action="" method="post" encType="multipart/form-data" className="form-horizontal">
                                <FormGroup row>
                                  <Col md="3">
                                    <Label>Projekt-ID</Label>
                                  </Col>
                                  <Col xs="12" md="9">
                                    <p className="form-control-static">{this.state.project.id}</p>
                                  </Col>
                                </FormGroup>
                                <FormGroup row>
                                  <Col md="3">
                                    <Label htmlFor="updatedProjectName">Projektname</Label>
                                  </Col>
                                  <Col xs="12" md="9">
                                    <Input type="text" id="updatedProjectName" name="updatedProjectName" value={updatedProjectName} onChange={this.handleChange} placeholder={this.state.project.name ? this.state.project.name : 'Projektname'} />
                                    <FormText color="muted">Geben Sie den gewünschten Projektnamen ein.</FormText>
                                  </Col>
                                </FormGroup>
                                <FormGroup row>
                                  <Col md="3">
                                    <Label htmlFor="updatedProjectDescription">Beschreibung</Label>
                                  </Col>
                                  <Col xs="12" md="9">
                                    <Input type="textarea" name="updatedProjectDescription" id="updatedProjectDescription" rows="9" value={updatedProjectDescription} onChange={this.handleChange}
                                          placeholder={this.state.project.description ? this.state.project.description : 'Beschreibung...'} />
                                  </Col>
                                </FormGroup>
                                <FormGroup row>
                                  <Col md="3">
                                    <Label htmlFor="file-input">Thumbnail</Label>
                                  </Col>
                                  <Col xs="12" md="9">
                                    <Input type="file" onChange={this.handleFileUploadChange} id="file-input" name="file-input" />
                                  </Col>
                                  <Col md="3">
                                  </Col>
                                  <Col xs="12" md="9">
                                    <img src={
                                      (() => {
                                        if(this.state.fileUrl) {
                                          return this.state.fileUrl;
                                        } else if(this.state.project.thumbnail) {
                                          return API_URL + this.state.project.thumbnail.url
                                        }
                                      })()
                                      //this.state.fileUrl ? this.state.fileUrl : ''
                                    } className="img-fluid img-thumbnail"/> 
                                  </Col>
                                </FormGroup>
                                <FormGroup row hidden>
                                  <Col md="3">
                                    <Label className="custom-file" htmlFor="custom-file-input">Custom file input</Label>
                                  </Col>
                                  <Col xs="12" md="9">
                                    <Label className="custom-file">
                                      <Input className="custom-file" type="file" id="custom-file-input" name="file-input" />
                                      <span className="custom-file-control"></span>
                                    </Label>
                                  </Col>
                                </FormGroup>
                              </Form>
                        </Col>
                      </Row>    
                    </ModalBody>
                    <ModalFooter>
                      <Button type="submit" color="primary" onClick={this.updateProjectViaApi}>Änderungen speichern</Button>{' '}
                      <Button color="secondary" onClick={this.toggle}>Abbrechen</Button>
                    </ModalFooter>
                  </Modal>
                </CardFooter>
              </Card>
            </Col>
          </Row>
          <Row>
            <Col>
              <Card>
                <CardHeader>
                  Wohneinheiten in <b>{this.state.project.name}</b>
                </CardHeader>
                <CardBody>
                  
                  <Row>
                    {unitCards}
                  </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
    );
  }
}

export default SingleProject;
