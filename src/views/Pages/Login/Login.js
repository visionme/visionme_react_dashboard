import React, { Component } from 'react';
import axios from 'axios'
import {API_URL} from '../../../utils/urls'
import crypto from 'crypto'

import { Link } from 'react-router-dom';
import { handleChange } from '../../../utils/inputs'
import { Alert } from 'reactstrap'

import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';

class Login extends Component {
  constructor(props) {
    super(props)
    
    this.state = {
      email: '',
      password: '',
      loginErrorVisible: false,
      roleErrorVisible: false
    }

    this.handleChange = handleChange.bind(this)
    this.onDismiss = this.onDismiss.bind(this);
  }

  componentDidMount(){
    if(localStorage.getItem('user')) {
      const { history } = this.props
      history.push('/')
    }
  }

  onDismiss() {
    this.setState({ 
      loginErrorVisible: false,
      roleErrorVisible: false,
    });
  }

  handleSubmit = async (event) => {
    // Prevent default behaviour (automatic page reload)
    event.preventDefault()
    console.log("Login.handleSubmit")
    // Sign in the user with strapi
    const {email, password} = this.state
    //const { history } = this.props
    const hashedPassword = crypto.createHash('sha256').update(password).digest('hex');

    const data = {
      identifier: email,
      password: hashedPassword
    }

    try{
      const userLoginRes = await axios({
        method: 'POST',
        url: `${API_URL}/auth/local`,
        data
      })
      
      console.log("Login.handleSubmit userLoginRes", userLoginRes)

      if(userLoginRes.data.user.role.name === 'Authenticated' || userLoginRes.data.user.role.name === 'Administrator') {
        localStorage.setItem('user', JSON.stringify(userLoginRes.data))
      } else {
        this.setState({roleErrorVisible: true})
        throw new Error("This is not an admin account.");
      }
      
      // Redirect to dashboard path: "/"
      //history.push('/')
      //return <Redirect to={{ pathname: '/' }} />
      window.location.reload(false);
    } 
    catch(error) {
      if(this.state.roleErrorVisible) {

      } else {
        this.setState({loginErrorVisible: true})
      }
      console.log(error)
    }
  }


  
  render() {
    const {email, password} = this.state

    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <Form onSubmit={this.handleSubmit}>
                      <h1>Login</h1>
                      <p className="text-muted">Melden Sie sich mit Ihrem Konto an.</p>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input name="email" id="email" type="text" value={email} placeholder="E-Mail" onChange={this.handleChange} autoComplete="username" />
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input name="password" id="password" type="password" value={password} placeholder="Passwort" onChange={this.handleChange} autoComplete="current-password" />
                      </InputGroup>
                      <Row>
                        <Alert color="warning" isOpen={this.state.loginErrorVisible} toggle={this.onDismiss}>
                          Email and password do not match.
                        </Alert>
                        <Alert color="warning" isOpen={this.state.roleErrorVisible} toggle={this.onDismiss}>
                          Please login with your admin account.
                        </Alert>
                      </Row>
                      <Row>
                        <Col xs="6">
                          <Button type="submit" color="primary" className="px-4">Login</Button>
                        </Col>
                        <Col xs="6" className="text-right">
                          <Button color="link" className="px-0">Passwort vergessen?</Button>
                        </Col>
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
                <Card className="text-white bg-primary py-5 d-md-down-none" style={{ width: '44%' }}>
                  <CardBody className="text-center">
                    <div>
                      <h2>Account erstellen</h2>
                      <p>Registrieren Sie sich und erhalten Sie Zugriff auf die weltweit erste Plattform für die Visualisierung von Immobilien.</p>
                      <Link to="/register">
                        <Button color="primary" className="mt-3" active tabIndex={-1}>Jetzt registrieren</Button>
                      </Link>
                    </div>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Login;
