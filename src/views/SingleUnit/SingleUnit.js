import React, { Component, lazy, Suspense, useState } from 'react';
import axios from 'axios'
import {API_URL} from '../../utils/urls'
import { handleChange } from '../../utils/inputs'
import { Bar, Line } from 'react-chartjs-2';
import {
  Badge,
  Button,
  ButtonDropdown,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  CardTitle,
  Col,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Progress,
  Row,
  Table,


  Modal, ModalBody, ModalFooter, ModalHeader,
  
  Collapse,
  Fade,
  Form,
  FormGroup,
  FormText,
  FormFeedback,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButtonDropdown,
  InputGroupText,
  Label,

} from 'reactstrap';



import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities'
import Axios from 'axios';

const brandPrimary = getStyle('--primary')
const brandSuccess = getStyle('--success')
const brandInfo = getStyle('--info')
const brandWarning = getStyle('--warning')
const brandDanger = getStyle('--danger')


class SingleUnit extends Component {
    constructor(props) {
      super(props);
  
      this.state = {
        // Modal states
        modal: false,
        variantModal: false,

        // File Upload Url state
        fileUrl: null,
        variantFileUrl: null,
        file: null,
        variantFile: null,

        // Other states
        user: '',
        token: '',
        unit: '',
        project: '',
        variants: '',
        selectedVariant: '',

        // new unit data
        updatedUnitName: '',
        updatedUnitDescription: '',

        // new variant data
        updatedVariantName: '',
        updatedVariantDescription: '',
      };

      this.toggle = this.toggle.bind(this);
      this.toggleVariantModal = this.toggleVariantModal.bind(this);
      this.handleFileUploadChange = this.handleFileUploadChange.bind(this);
      this.handleVariantFileUploadChange = this.handleVariantFileUploadChange.bind(this);
      this.handleChange = handleChange.bind(this);
    }
    
    toggle() {
      this.setState({
        modal: !this.state.modal,
      });
    }

    toggleVariantModal(id) {
      console.log('Variant id: ', id)
      if(typeof id === typeof 0) {
        const variant = this.state.variants[id];
        this.setState({
          selectedVariant: variant,
          updatedVariantName: variant.name,
          updatedVariantDescription: variant.description,
          variantModal: !this.state.variantModal,
        })
      } else {
        this.setState({
          variantModal: !this.state.variantModal,
        })
      }
    }

    // Start
    componentDidMount() {
      console.log('URL Params: ', this.props.match.params)
      this.getUnitFromApi();
      const { history } = this.props
    }

    getUnitFromApi = async () => {
      const userObject = JSON.parse(localStorage.getItem('user'));
      
      try{
        const unitRes = await axios({
          method: 'GET',
          url: `${API_URL}/units/${this.props.match.params.unitid}`,
          headers: {
            'Authorization': `Bearer ${userObject.jwt}`
          }
        })
        
        console.log("SingleUnit.js getUnitFromApi", unitRes.data)
  
        this.setState({
          unit: unitRes.data,
          project: unitRes.data.project,
          variants: unitRes.data.variants,
          updatedUnitName: unitRes.data.name,
          updatedUnitDescription: unitRes.data.description,
        }, () => {
          console.log('unit:' + JSON.stringify(this.state.unit))
        })
      } 
      catch(error) {
        console.log(error)
      }
    }

    handleFileUploadChange(event) {
      this.setState({
        fileUrl: URL.createObjectURL(event.target.files[0]),
        file: event.target.files[0],
      })
    }

    handleVariantFileUploadChange(event) {
      this.setState({
        variantFileUrl: URL.createObjectURL(event.target.files[0]),
        variantFile: event.target.files[0],
      })
    }
    
    updateUnitViaApi = async () => {
      const userObject = JSON.parse(localStorage.getItem('user'));
      
      const updatedUnit = {
        name: this.state.updatedUnitName,
        description: this.state.updatedUnitDescription,
      }
      
      // construct new preview image json object
      const formData = new FormData();
      formData.append('files', this.state.file);
      formData.append('ref', 'unit');
      formData.append('refId', this.state.unit.id);
      formData.append('field', 'thumbnail');


      console.log('Updated Unit Object: ', updatedUnit)
      console.log('Form data object: ', formData)

      try{
        // Update Unit info
        console.log('POST: update product')
        const updateUnitRes = await Axios({
          method: 'PUT',
          url: `${API_URL}/units/${this.state.unit.id}`,
          headers: {
            'Authorization': `Bearer ${userObject.jwt}`
          },
          data: updatedUnit,
        })
        console.log("SingleUnit.js updateUnitViaApi Response: ", updateUnitRes.data);
        this.setState({
          unit: updateUnitRes.data
        });
        


        // Update unit preview image
        if(this.state.file) {
          console.log('POST: update product thumbnail')

          const updateImageRes = await Axios({
            method: 'POST',
            url: `${API_URL}/upload`,
            headers: {
              'Authorization': `Bearer ${userObject.jwt}`
            },
            data: formData,
          })

          console.log('UpdateImage Response: ', updateImageRes)
          window.location.reload(false);
        }


        this.toggle();
      } catch(error) {
        console.log('Error: ', error.message);
      }
    }

    updateVariantViaApi = async () => {
      const userObject = JSON.parse(localStorage.getItem('user'));
      
      const updatedVariant = {
        name: this.state.updatedVariantName,
        description: this.state.updatedVariantDescription,
      }
      
      // construct new preview image json object
      const formData = new FormData();
      formData.append('files', this.state.variantFile);
      formData.append('ref', 'variant');
      formData.append('refId', this.state.selectedVariant.id);
      formData.append('field', 'thumbnail');


      console.log('Updated Unit Object: ', updatedVariant)
      console.log('Form data object: ', formData)

      try{
        // Update Variant info
        console.log('POST: update variant')
        const updateVariantRes = await Axios({
          method: 'PUT',
          url: `${API_URL}/variants/${this.state.selectedVariant.id}`,
          headers: {
            'Authorization': `Bearer ${userObject.jwt}`
          },
          data: updatedVariant,
        })
        console.log("SingleUnit.js updateVariantViaApi Response: ", updateVariantRes.data);

        // Get old variants array and replace the updated variant
        let newVariants = this.state.variants;
        newVariants[this.state.selectedVariant.id] = updateVariantRes.data;

        // Set new variants array to state
        this.setState({
          variants: newVariants,
        });
        


        // Update unit preview image
        if(this.state.variantFile) {
          console.log('POST: update variant thumbnail')

          const updateImageRes = await Axios({
            method: 'POST',
            url: `${API_URL}/upload`,
            headers: {
              'Authorization': `Bearer ${userObject.jwt}`
            },
            data: formData,
          })

          console.log('UpdateImage Response: ', updateImageRes)
        }
        
        
        window.location.reload(false);
        this.toggleVariantModal();
      } catch(error) {
        console.log('Error: ', error.message);
      }
    }

    loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>
  
    render() {
      const {updatedUnitName, updatedUnitDescription} = this.state;
      const {updatedVariantName, updatedVariantDescription} = this.state;
      var variantListItems = '';
      if(typeof this.state.variants === typeof [1,2]) {
        variantListItems = this.state.variants.map((variant, index) => {
          return (
            <tr key={variant.id} className="table-bordered">
              <td className="text-center">
                <div>
                  <img src={variant.thumbnail ? `${API_URL}${variant.thumbnail.url}` : ''} height="60" className="variant-image" alt=""/>
                </div>
              </td>
              <td>
                <b>{variant.name}</b>
                <br></br>
                {variant.description}
                <br></br>
                <div className="small text-muted">
                  <span>Neu</span> | Erstellt: {variant.createdAt}
                </div>
              </td>
              <td className="text-center">
                <Badge className="mr-1" color="success">Verfügbar</Badge>
              </td>
              <td>
                <Button block color="primary" size="sm" onClick={() => {
                  window.open('/#/starter', "_self")
                  //window.open(`/#/projects/${this.state.project.id}/${unit.id}`, "_self")
                }}>Archivieren</Button>
                <Button block color="primary" size="sm" onClick={() => {this.toggleVariantModal(index)}}>Bearbeiten</Button>
              </td>
            </tr>
          )
        })
      }
      
      /*var variantRowItems = '';
      console.log('Variants in this.state: ', this.state.variants)
      if(typeof this.state.variants === typeof [1,2]) {
        variantRowItems = this.state.variants.map(variant => {
          return (
            <Row>
              <Col xs="1">
                <img alt="" className="img-fluid" src={`${API_URL}${variant.thumbnail.url}`} />
              </Col>
              <Col xs="7">
                <p>{variant.name}</p>
              </Col>
              <Col xs="2" className="text-center">
                <Badge className="mr-1" color="success">Verfügbar</Badge>
              </Col>
              <Col xs="2" className="text-center">
                <Button block color="primary" size="sm" onClick={() => {
                  window.open('/#/starter', "_self")
                  //window.open(`/#/projects/${this.state.project.id}/${unit.id}`, "_self")
                }}>Öffnen</Button>
              </Col>
            </Row>
          )
        })
      }*/
      

      return (
        <div className="animated fadeIn">

          <Row>
            <Col>
              <Card>
                <CardHeader>
                  Projekt: {this.state.project.name} / Wohneinheit: {this.state.unit.name ? this.state.unit.name : "Dieses Projekt wurde noch nicht benannt." }
                </CardHeader>
                <CardBody>
                <Row>
                    <Col xs="7" xl="8">
                      <Row>
                        <Col xs="4" xl="3">
                          <div>
                            <p>
                              <b>Name:</b>
                            </p>
                          </div>
                        </Col>
                        <Col xs="8" xl="9">
                          <div>
                            <p>
                            {this.state.unit.name ? this.state.unit.name : "Für diese Wohneinheit wurde kein Name hinterlegt." }
                            </p>
                          </div>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="4" xl="3">
                          <div>
                            <p>
                            <b>Beschreibung:</b>
                            </p>
                          </div>
                        </Col>
                        <Col xs="8" xl="9">
                          <div>
                            <p>
                              {this.state.unit.description ? this.state.unit.description : "Für diese Wohneinheit wurde keine Beschreibung hinterlegt.    " }
                            </p>
                          </div>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="4" xl="3">
                          <div>
                            <p>
                            <b>Erstellt am:</b>
                            </p>
                          </div>
                        </Col>
                        <Col xs="8" xl="9">
                          <div>
                            <p>
                              {this.state.unit.createdAt ? this.state.unit.createdAt : 'Keine Angabe.'}
                            </p>
                          </div>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="4" xl="3">
                          <div>
                            <p>
                            <b>Anzahl m²:</b>
                            </p>
                          </div>
                        </Col>
                        <Col xs="8" xl="9">
                          <div>
                            <p>
                              {this.state.unit.squareMetres ? this.state.unit.squareMetres : 'Nicht verfügbar'}
                            </p>
                          </div>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="4" xl="3">
                          <div>
                            <p>
                              <b>Gespeicherte Einrichtungen:</b>
                            </p>
                          </div>
                        </Col>
                        <Col xs="8" xl="9">
                          <div>
                            <p>
                              {this.state.unit.variants ? this.state.unit.variants.length : 0}
                            </p>
                          </div>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="4" xl="3">
                          <div>
                            <p>
                              <b>Status:</b>
                            </p>
                          </div>
                        </Col>
                        <Col xs="8" xl="9">
                          <Badge className="mr-1" color="success">Verfügbar</Badge>
                        </Col>
                      </Row>
                    </Col>
                    <Col xs="5" xl="4" className="text-right overflow">
                      <div>
                        <img src={this.state.unit.thumbnail ? `${API_URL}${this.state.unit.thumbnail.url}` : ''} height="250" className="border overflow-hidden" alt=""/>
                      </div>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <Button color="primary" onClick={this.toggle} className="mr-1">Bearbeiten</Button>
                  <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                    <ModalHeader toggle={this.toggle}>Wohneinheit bearbeiten</ModalHeader>
                    <ModalBody>
                      <Row>
                        <Col xs="12" md="12">
                          
                              <Form onSubmit={this.updateUnitViaApi} action="" method="post" encType="multipart/form-data" className="form-horizontal">
                                <FormGroup row>
                                  <Col md="3">
                                    <Label>ID</Label>
                                  </Col>
                                  <Col xs="12" md="9">
                                    <p className="form-control-static">{this.state.unit.id}</p>
                                  </Col>
                                </FormGroup>
                                
                                <FormGroup row>
                                  <Col md="3">
                                    <Label htmlFor="updatedUnitName">Name</Label>
                                  </Col>
                                  <Col xs="12" md="9">
                                    <Input type="text" id="updatedUnitName" name="updatedUnitName" value={updatedUnitName} onChange={this.handleChange} placeholder={this.state.unit.name ? this.state.unit.name : 'Name Wohnungseinheit'} />
                                    <FormText color="muted">Geben Sie den gewünschten Projektnamen ein.</FormText>
                                  </Col>
                                </FormGroup>
                                
                                <FormGroup row>
                                  <Col md="3">
                                    <Label htmlFor="updatedUnitDescription">Beschreibung</Label>
                                  </Col>
                                  <Col xs="12" md="9">
                                    <Input type="textarea" name="updatedUnitDescription" id="updatedUnitDescription" rows="9" value={updatedUnitDescription} onChange={this.handleChange}
                                          placeholder={this.state.unit.description ? this.state.unit.description : 'Beschreibung...'} />
                                  </Col>
                                </FormGroup>
                                
                                <FormGroup row>
                                  <Col md="3">
                                    <Label htmlFor="file-input">Thumbnail</Label>
                                  </Col>
                                  <Col xs="12" md="9">
                                    <Input type="file" onChange={this.handleFileUploadChange} id="file-input" name="file-input" />
                                  </Col>
                                  <Col md="3">
                                  </Col>

                                  <Col xs="12" md="9">
                                    <img src={
                                      (() => {
                                        if(this.state.fileUrl) {
                                          return this.state.fileUrl;
                                        } else if(this.state.unit.thumbnail) {
                                          return API_URL + this.state.unit.thumbnail.url
                                        }
                                      })()
                                      //this.state.fileUrl ? this.state.fileUrl : ''
                                    } className="img-fluid img-thumbnail"/> 
                                  </Col>
                                </FormGroup>
                              </Form>
                            
                          
                        </Col>
                      </Row>    
                    </ModalBody>
                    <ModalFooter>
                      <Button type="submit" color="primary" onClick={this.updateUnitViaApi}>Änderungen speichern</Button>{' '}
                      <Button color="secondary" onClick={this.toggle}>Abbrechen</Button>
                    </ModalFooter>
                  </Modal>
                </CardFooter>
              </Card>
            </Col>
          </Row>

          <Row>
            <Col>
              <Card>
                <CardHeader>
                  <Row>
                    <Col className="my-auto">
                      Gespeicherte Einrichtungen (Stagings) in <b>{this.state.unit.name}</b>
                    </Col>
                    <Col className="text-right">
                      <Button color="primary">Neues Staging anfordern</Button>
                    </Col>
                  </Row>
                </CardHeader>
                <CardBody>
                  <Row>
                    <Table hover responsive striped size="sm" className="table-outline mb-0 d-none d-sm-table">
                      <thead className="thead-light">
                        <tr>
                          <th className="text-left col-xs-1">Anzeigebild</th>
                          <th className="text-left col-xs-8">Infos</th>
                          <th className="text-left col-xs-1">Status</th>
                          <th className="text-left col-xs-2">Aktionen</th>
                        </tr>
                      </thead>
                      <tbody>{variantListItems}</tbody>
                    </Table>
                  </Row>
                </CardBody>
                <Modal isOpen={this.state.variantModal} toggle={this.toggleVariantModal} className={this.props.className}>
                    <ModalHeader toggle={this.toggleVariantModal}>Einrichtung bearbeiten</ModalHeader>
                    <ModalBody>
                      <Row>
                        <Col xs="12" md="12">
                          <Form onSubmit={this.updateUnitViaApi} action="" method="post" encType="multipart/form-data" className="form-horizontal">
                            <FormGroup row>
                              <Col md="3">
                                <Label>Staging-ID</Label>
                              </Col>
                              <Col xs="12" md="9">
                                <p className="form-control-static">{this.state.selectedVariant.id}</p>
                              </Col>
                            </FormGroup>
                            
                            <FormGroup row>
                              <Col md="3">
                                <Label htmlFor="updatedVariantName">Name</Label>
                              </Col>
                              <Col xs="12" md="9">
                                <Input type="text" id="updatedVariantName" name="updatedVariantName" value={updatedVariantName} onChange={this.handleChange} placeholder={this.state.unit.name ? this.state.unit.name : 'Name Staging'} />
                                <FormText color="muted">Geben Sie den gewünschten Einrichtungsnamen ein.</FormText>
                              </Col>
                            </FormGroup>
                            
                            <FormGroup row>
                              <Col md="3">
                                <Label htmlFor="updatedVariantDescription">Beschreibung</Label>
                              </Col>
                              <Col xs="12" md="9">
                                <Input type="textarea" name="updatedVariantDescription" id="updatedVariantDescription" rows="9" value={updatedVariantDescription} onChange={this.handleChange}
                                      placeholder={this.state.selectedVariant.description ? this.state.selectedVariant.description : 'Beschreibung...'} />
                              </Col>
                            </FormGroup>
                            
                            <FormGroup row>
                              <Col md="3">
                                <Label htmlFor="file-input">Thumbnail</Label>
                              </Col>
                              <Col xs="12" md="9">
                                <Input type="file" onChange={this.handleVariantFileUploadChange} id="file-input" name="file-input" />
                              </Col>
                              <Col md="3">
                              </Col>

                              <Col xs="12" md="9">
                                <img src={
                                  (() => {
                                    if(this.state.variantFileUrl) {
                                      return this.state.variantFileUrl;
                                    } else if(this.state.selectedVariant.thumbnail) {
                                      return API_URL + this.state.selectedVariant.thumbnail.url
                                    }
                                  })()
                                  //this.state.fileUrl ? this.state.fileUrl : ''
                                } className="img-fluid img-thumbnail"/> 
                              </Col>
                            </FormGroup>
                          </Form>
                        </Col>
                      </Row>    
                    </ModalBody>
                    <ModalFooter>
                      <Button type="submit" color="primary" onClick={this.updateVariantViaApi}>Änderungen speichern</Button>{' '}
                      <Button color="secondary" onClick={this.toggleVariantModal}>Abbrechen</Button>
                    </ModalFooter>
                  </Modal>

              </Card>
            </Col>
          </Row>
        </div>
    );
  }
}

export default SingleUnit;
