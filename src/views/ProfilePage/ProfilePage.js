import React, { Component, lazy, Suspense } from 'react';
import axios from 'axios';
import {API_URL} from '../../utils/urls';
import { handleChange } from '../../utils/inputs';
import Axios from 'axios';
import {
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  CardTitle,
  Col,
  Row,
  Badge,
  Nav,
  NavItem,
  NavLink,
  TabContent,
  TabPane,
  Form,
  FormGroup,
  Label,
  Input,
  Button,
  Alert,
} from 'reactstrap';

class ProfilePage extends Component {
    constructor(props) {
      super(props);
  
      this.state = {
        activeTab: new Array(4).fill('1'),
        user: '',
        username: '',
        email: '',

        // Updated email data
        newEmail: '',
        newEmailRepeat: '',

        // Company tab
        fileUrl: '',
        file: '',

        updatedCompanyName: '',
        updatedCompanyColorcode: '#4169E1',
        updatedCompanyLogo: '',

        // Security page
        oldPassword: '',
        newPassword: '',
        newPasswordRepeat: '',

        // Alert fields toggle
        noValidEmailAlertVisible: false,
        mailsDontMatchAlertVisible: false,
        successAlertVisible: false,
      };

      this.toggle = this.toggle.bind(this);
      this.handleChange = handleChange.bind(this);
      this.handleFileUploadChange = this.handleFileUploadChange.bind(this);
    }

    loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>
  
    toggle(tabPane, tab) {
        const newArray = this.state.activeTab.slice()
        newArray[tabPane] = tab
        this.setState({
          activeTab: newArray,
        });
    }
    
    // Start
    componentDidMount() {
        this.getUserFromApi();
        const { history } = this.props
    }

    onDismiss = () => {
        this.setState({ 
            noValidEmailAlertVisible: false,
            mailsDontMatchAlertVisible: false,
        });
    }

    getUserFromApi = async () => {
        const userObject = JSON.parse(localStorage.getItem('user'));
        
        try{
          const userRes = await axios({
            method: 'GET',
            url: `${API_URL}/users/me`,
            headers: {
              'Authorization': `Bearer ${userObject.jwt}`
            }
          })
          
          console.log("ProfilePage.js getUserFromApi", userRes.data)

          const updatedCompanyName = userRes.data.companyName ? userRes.data.companyName : this.state.updatedCompanyName;
          const updatedCompanyColorcode = userRes.data.companyColor ? userRes.data.companyColor : this.state.updatedCompanyColorcode;
          const updatedCompanyLogo = userRes.data.companyLogo ? userRes.data.companyLogo : this.state.updatedCompanyLogo;


          this.setState({
            user: userRes.data,
            username: userRes.data.username,
            email: userRes.data.email,
            updatedCompanyName,
            updatedCompanyColorcode,
            updatedCompanyLogo,
          })

          if(userRes.data.companyLogo) {
            this.setState({
              fileUrl: userRes.data.companyLogo.url,
            })
          }
        } 
        catch(error) {
          console.log(error)
        }
    }


    handleFileUploadChange(event) {
      this.setState({
        fileUrl: URL.createObjectURL(event.target.files[0]),
        file: event.target.files[0],
      })
    }


    // Update user API call: [PUT] "/users/me"
    updateOwnUserViaApi = async () => {
      const userObject = JSON.parse(localStorage.getItem('user'));

      try{
        // Compare user.username and this.state.username // Same with email
        // Then append the changed value to updateUser
        const updatedUser = {}
  
        if(this.state.user.username !== this.state.username) {
          updatedUser.username = this.state.username;
        }
        if(this.state.newEmail !== '' ) {
          if(this.ValidateEmail(this.state.newEmail)) {
            if(this.state.newEmail === this.state.newEmailRepeat) {
              updatedUser.email = this.state.newEmail;
            } else {
              this.setState({
                mailsDontMatchAlertVisible: true,
              }, ()=> {window.setTimeout(()=>{this.setState({mailsDontMatchAlertVisible:false})},5000)});
              throw new Error('Die angegebenen E-Mail Adressen stimmen nicht überein.');
            }
          } else {
            this.setState({
              noValidEmailAlertVisible: true,
            }, ()=> {window.setTimeout(()=>{this.setState({noValidEmailAlertVisible:false})},5000)});
            throw new Error('Bitte geben Sie eine valide E-Mail-Adresse ein.')
          }
        }

        // Update User
        console.log('Update user: [PUT] ', updatedUser)
        const updateUserRes = await Axios({
          method: 'PUT',
          url: `${API_URL}/users/me`,
          headers: {
            'Authorization': `Bearer ${userObject.jwt}`
          },
          data: updatedUser,
        })
        console.log("ProfilePage.js updateOwnUserViaApi Response: ", updateUserRes.data);
        this.setState({
          // update local user data
          user: updateUserRes.data,
          username: updateUserRes.data.username,
          email: updateUserRes.data.email,

          // Reset input fields
          newEmail: '',
          newEmailRepeat: '',

          successAlertVisible: true,
        }, ()=> {window.setTimeout(()=>{this.setState({successAlertVisible:false})},8000)});
      } catch(error) {
        console.log('Error: ', error.message);
      }
    }



    updateUsersCompanyViaApi = async () => {
      const userObject = JSON.parse(localStorage.getItem('user'));

      
      const updatedUser = {}
      
      if( !(this.state.updatedCompanyName === '') && !( this.state.updatedCompanyName === this.state.user.companyName) ) {
        updatedUser.companyName = this.state.updatedCompanyName;
      }
      if( !(this.state.updatedCompanyColorcode === '') && !( this.state.updatedCompanyColorcode === this.state.user.companyColor) ) {
        updatedUser.companyColor = this.state.updatedCompanyColorcode;
      }

      // construct new preview image json object
      const formData = new FormData();
      formData.append('files', this.state.file);
      formData.append('ref', 'user');
      formData.append('refId', this.state.user.id);
      formData.append('field', 'companyLogo');


      console.log('Updated user Object: ', updatedUser)
      console.log(...formData)

      try{
        if(!this.isEmpty(updatedUser)) {
          // Update Project info
          console.log('PUT: update users company')
          const updateUserRes = await Axios({
            method: 'PUT',
            url: `${API_URL}/users/me`,
            headers: {
              'Authorization': `Bearer ${userObject.jwt}`
            },
            data: updatedUser,
          })
          console.log("SingleProject.js updateProjectViaApi Response: ", updateUserRes.data);
          this.setState({
            updatedCompanyName: updateUserRes.data.company.name,
            updatedCompanyColorcode: updateUserRes.data.company.colorcode,
          });
        }
        


        // Update user.company logo image
        if(this.state.file) {
          console.log('POST: update user.companyName etc.')

          const updateImageRes = await Axios({
            method: 'POST',
            url: `${API_URL}/upload`,
            headers: {
              'Authorization': `Bearer ${userObject.jwt}`
            },
            data: formData,
          })

          console.log('UpdateImage Response: ', updateImageRes)
          // this.setState({..}) is better than reload.. 
          window.location.reload(false);
        }
      } catch(error) {
        console.log('Error: ', error.message);
      }
    }



    ValidateEmail(inputText) {
      var oldMailformat = /^w+([.-]?w+)*@w+([.-]?w+)*(.w{2,3})+$/;
      var mailformat = /\S+@\S+\.\S+/;
      if(inputText.match(mailformat))
      {
        return true;
      }
      else
      {
        return false;
      }
    }

    isEmpty(obj) {
      for(var key in obj) {
          if(obj.hasOwnProperty(key))
              return false;
      }
      return true;
    }

    tabPane() {
        const { username, email, newEmail, newEmailRepeat, updatedCompanyName, updatedCompanyColorcode, updatedCompanyLogo, oldPassword, newPassword, newPasswordRepeat } = this.state;

        return (
            <>
            <TabPane tabId="1">
                <h4>Accountdaten ändern</h4>
                <br></br>
                <Form>
                <FormGroup>
                <Label htmlFor="username">Accountname</Label>
                <Input type="text" name="username" id="username" value={username} onChange={this.handleChange} placeholder="" />
                </FormGroup>
                <FormGroup>
                <Label htmlFor="email">E-Mail</Label>
                <Input type="text" name="email" id="email" value={email} readOnly placeholder="" />
                </FormGroup>
                <FormGroup>
                <Label htmlFor="newEmail">Neue E-Mail</Label>
                <Input type="text" name="newEmail" id="newEmail" value={newEmail} onChange={this.handleChange} placeholder="" />
                </FormGroup>
                <FormGroup>
                <Label htmlFor="newEmailRepeat">Neue E-Mail bestätigen</Label>
                <Input type="text" name="newEmailRepeat" id="newEmailRepeat" value={newEmailRepeat} onChange={this.handleChange} placeholder="" />
                </FormGroup>
                  <Alert color="warning" isOpen={this.state.mailsDontMatchAlertVisible} toggle={this.onDismiss}>
                    Die angegebenen E-Mail Adressen stimmen nicht überein.
                  </Alert>
                  <Alert color="warning" isOpen={this.state.noValidEmailAlertVisible} toggle={this.onDismiss}>
                    Bitte geben Sie eine valide E-Mail-Adresse ein.
                  </Alert>
                  <Alert color="success" isOpen={this.state.successAlertVisible} toggle={this.onDismiss}>
                    Die Änderungen wurden erfolgreich übernommen.
                  </Alert>
                </Form>
                <br></br>
                <div className="text-right">
                    <Button color="primary" onClick={this.updateOwnUserViaApi}>Speichern</Button>
                </div>
                
            </TabPane>
            <TabPane tabId="2"> 
                <h4>Unternehmensdaten ändern</h4>
                <p>Diese Daten werden genutzt, um den Immobilien-Designer an Ihr Corporate Design anzupassen.</p>
                <br></br>
                <FormGroup>
                <Label htmlFor="updatedCompanyName">Name Ihres Unternehmens</Label>
                <Input type="text" name="updatedCompanyName" id="updatedCompanyName" value={updatedCompanyName} onChange={this.handleChange} placeholder="" />
                </FormGroup>
                <FormGroup row>
                <Col xs="8">
                  <Label htmlFor="updatedCompanyColorcode">Primärfarbe Ihres Unternehmens</Label>
                  <Input type="color" name="updatedCompanyColorcode" id="updatedCompanyColorcode" value={updatedCompanyColorcode} onChange={this.handleChange} placeholder="#FFFFFF" />
                </Col>
                <Col xs="4">
                  <Label htmlFor="updatedCompanyColorcode">Farbcode</Label>
                  <Input type="text" name="updatedCompanyColorcode" id="updatedCompanyColorcode" value={updatedCompanyColorcode} onChange={this.handleChange} placeholder="#FFFFFF" />
                </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs="12">
                    <Label htmlFor="file-input">Logo</Label>
                  </Col>
                  <Col xs="12" md="9">
                    <Input type="file" onChange={this.handleFileUploadChange} id="file-input" name="file-input" />
                  </Col>
                  <Col md="3">
                  </Col>
                  <Col xs="12" md="9">
                    <img src={
                      (() => {
                        if(this.state.fileUrl) {
                          return this.state.fileUrl;
                        } else if(this.state.updatedCompanyLogo) {
                          return API_URL + this.state.updatedCompanyLogo.url
                        }
                      })()
                      //this.state.fileUrl ? this.state.fileUrl : ''
                    } className="img-fluid img-thumbnail"/> 
                  </Col>
                </FormGroup>

                <br></br>
                <div className="text-right">
                    <Button color="primary" onClick={this.updateUsersCompanyViaApi}>Speichern</Button>
                </div>
            </TabPane>
            <TabPane tabId="3">
                <FormGroup>
                <h4>Passwort ändern</h4>
                <br></br>
                <Label htmlFor="oldPassword">Aktuelles Passwort:</Label>
                <Input type="password" name="oldPassword" id="oldPassword" value={oldPassword} onChange={this.handleChange} placeholder="" />
                </FormGroup>
                <FormGroup>
                <Label htmlFor="newPassword">Neues Passwort:</Label>
                <Input type="password" name="newPassword" id="newPassword" value={newPassword} onChange={this.handleChange} placeholder="" />
                </FormGroup>
                <FormGroup>
                <Label htmlFor="newPasswordRepeat">Neues Passwort wiederholen:</Label>
                <Input type="password" name="newPasswordRepeat" id="newPasswordRepeat" value={newPasswordRepeat} onChange={this.handleChange} placeholder="" />
                </FormGroup>
                <br></br>
                <div className="text-right">
                    <Button color="primary">Speichern</Button>
                </div>
            </TabPane>
            </>
        );
    }

    render() {
     

      return (
        <div className="animated fadeIn">
          <Row>
            <Col xs="12" md="6" className="mb-4">
                <Nav tabs>
                    <NavItem>
                        <NavLink
                        active={this.state.activeTab[0] === '1'}
                        onClick={() => { this.toggle(0, '1'); }}
                        >
                        Account
                        </NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink
                        active={this.state.activeTab[0] === '2'}
                        onClick={() => { this.toggle(0, '2'); }}
                        >
                        Unternehmen
                        </NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink
                        active={this.state.activeTab[0] === '3'}
                        onClick={() => { this.toggle(0, '3'); }}
                        >
                        Sicherheit
                        </NavLink>
                    </NavItem>
                </Nav>
                <TabContent activeTab={this.state.activeTab[0]}>
                {this.tabPane()}
                </TabContent>
            </Col>
          </Row>
        </div>
    );
  }
}

export default ProfilePage;